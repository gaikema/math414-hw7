# Math 414 Homework 7
Math 414 [homework 7](http://www.math.tamu.edu/~francis.narcowich/m414/s16/m414s16_hwc.html)

---

# Resources

## DFT
* [Plotting](http://www.phys.nsu.ru/cherk/fft.pdf)
* [Plotting in R](https://onestepafteranother.wordpress.com/signal-analysis-and-fast-fourier-transforms-in-r/)

## Problem 1
* Theorem 1.3: page 44
* Problem 25: page 86, solution on page 291

## Examples
* http://www.math.tamu.edu/~fnarc/m414/prob_4_1.pdf
* [4.6 and 4.7](http://www.math.tamu.edu/~fnarc/m414/s16/m414s16_prob_4-6_4-7.html)
* http://www.math.tamu.edu/~fnarc/m414/s15/example_4_11.pdf

## C++
* [Algortithm for calculating Haar Coefficients](https://bitbucket.org/gaikema/haar)

## CSV
* [Reading csv from url](http://stackoverflow.com/questions/14441729/read-a-csv-from-github-into-r)